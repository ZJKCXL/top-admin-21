<h1>  <a href="#" class="far fa-arrow-alt-circle-left"  onclick="window.history.back();" title='Zpět'></a>  Informace o hodnotiteli</h1>
<style>
.input-group-prepend, .input-group-text {
   min-width: 160px; 
}
</style>
<?php 
$table_name = "an_tbluser";
$page_name = "anusers";
$priviledge_arr = false;

if (isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
	$user_id = $_REQUEST["newsid"];
}
else
{
	$user_id = false;
	$row["UCanLogin"] = 1;
}

if ($user_id)
{
	$query = "Select * from ".$table_name." where ID = ".$user_id." and Deleted = 0";
	$res = mysql_query($query);

	if ($res && @mysql_num_rows($res)>0)
	{
		$row = @mysql_fetch_array($res);
	}

	$query = "Select PUPriviledge from tblpriviledgeuser_admin where PUUser = ".$user_id." and Deleted = 0";
	$res = mysql_query($query);

	if ($res &&  mysql_num_rows($res)>0)
	{
		$priviledge_arr = array();
		while ($priv_row = @mysql_fetch_array($res))
		{
			$priviledge_arr[] = $priv_row["PUPriviledge"];
		}
	}

}

?>
<form action="index.php?id=<?php  echo $page_name; ?><?php if($user_id>0){echo "&newsid=",$user_id;}?>" method="post" class="container"  ENCTYPE="multipart/form-data">
<?php 
if ($user_id)
{
?>
    <input type="hidden" name="user_id" value="<?php  echo $user_id; ?>" />
<?php 
}
?>


<div class="row">

	<fieldset class="col">
	
	
	
	
	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Jméno</span>
    </div>
    <input type="text"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["UName"]; ?>" name="user_name" />
    </div>

	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Příjmení</span>
    </div>
    <input type="text"  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["USurname"]; ?>" name="user_surname" />
    </div>

	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Společnost</span>
    </div>
    <input type="text" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["UHis"]; ?>" name="UHis" />
	</div>
	
	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Přihlašovací jméno (E-mail)</span>
    </div>
    <input type="text" required class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="<?php  echo $row["ULogin"]; ?>" name="user_login"  />
    </div>	

	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Heslo</span>
    </div>
    <input type="text" <?php  if (!$user_id) { echo "required";} ?>  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="" name="user_pass"  />
	</div>	
	
	<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">Heslo (opakovat)</span>
    </div>
    <input type="text"  <?php  if (!$user_id) { echo "required";} ?>  class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" value="" name="user_pass_repeat"   />
    </div>	

 
	
    </fieldset>

</div>
 
 <div class="custom-control custom-checkbox my-1 mr-sm-2">
 
	<input type="checkbox" name="can_login" class="custom-control-input" id="customControlInline" <?php  if ($row["UCanLogin"] == 1) { echo "checked";} ?>   />
    <label class="custom-control-label" for="customControlInline">Smí se přihlásit</label>
  </div>
 <br/>

  <div class='sender'>       
  <input type="submit" class="btn btn-primary " value="Uložit uživatele" name="send"> 
  </div>
 
</form>

 