<a href="#" id="btnExport"> EXCEL > </a>
<script type="text/javascript">
    $("#btnExport").click(function (e) {
        var htmltable= document.getElementById('tableOUT');
        var html = htmltable.outerHTML;
        while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
        while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;');
        while (html.indexOf('č') != -1) html = html.replace('č', '&#269;');
        while (html.indexOf('Č') != -1) html = html.replace('Č', '&#268;');        
        while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
        while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;');
        while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
        while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;');
        while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
        while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;');
        while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
        while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;');
        while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
        while (html.indexOf('ň') != -1) html = html.replace('ň', '&#328;'); 
        while (html.indexOf('Ň') != -1) html = html.replace('Ň', '&#327;');
        while (html.indexOf('š') != -1) html = html.replace('š', '&#353;'); 
        while (html.indexOf('Š') != -1) html = html.replace('Š', '&#352;');  
        while (html.indexOf('ř') != -1) html = html.replace('ř', '&#345;'); 
        while (html.indexOf('Ř') != -1) html = html.replace('Ř', '&#344;');    
        while (html.indexOf('ť') != -1) html = html.replace('ť', '&#357;'); 
        while (html.indexOf('Ť') != -1) html = html.replace('Ť', '&#356;');   
        while (html.indexOf('´') != -1) html = html.replace('´', '&#39;');  
        while (html.indexOf('ě') != -1) html = html.replace('ě', '&#283;');  
        while (html.indexOf('Ě') != -1) html = html.replace('Ě', '&#282;');  
        while (html.indexOf('Ý') != -1) html = html.replace('Y', '&Yacute;');
        while (html.indexOf('ý') != -1) html = html.replace('ý', '&yacute;');
        while (html.indexOf('ž') != -1) html = html.replace('ž', '&#382;');
        while (html.indexOf('Ž') != -1) html = html.replace('Ž', '&#381;');    
        while (html.indexOf('ů') != -1) html = html.replace('ů', '&#367;');   
        while (html.indexOf('–') != -1) html = html.replace('–', '-');   
        while (html.indexOf('…') != -1) html = html.replace('…', '...');   

        var result = 'data:application/vnd.ms-excel,' + encodeURIComponent(html);
        this.href = result;
       
        this.download =  "<?php echo $excelName; ?>";
        return true;
    });
</script> 